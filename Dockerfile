FROM node:16-alpine

WORKDIR /src

COPY package.json package-lock.json /src/

RUN npm install

COPY . .

RUN npm run build

EXPOSE 3000

CMD ["npm","start"]




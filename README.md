      
# DataWatch UI

## Install dependencies
```
npm install
```

## Configure
Copy `.env` to `.env.local` and modify accordingly.

For local development use these Keycloak settings:
```
KEYCLOAK_URL=https://kc.cyverse.org/auth/
KEYCLOAK_REALM=CyVerse
KEYCLOAK_CLIENT=datawatch-local
KEYCLOAK_SECRET=<secret>
```

## Run server
```
# Local development
npm run dev

# Production environment
npm run start
```

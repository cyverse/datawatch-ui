require("dotenv").config({ path: __dirname + "/./../../.env.local" });
const auth = require("./auth");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const DataWatchAPI = require("./apiClient");
const express = require("express");
// const navRouter = require("./routes/routes");
const next = require("next");
const Sentry = require("@sentry/node");
// const session = require("express-session");
// const memoryStore = new session.MemoryStore();
// const Keycloak = require("keycloak-connect");
// const Sentry = require("@sentry/node");
// const Tracing = require("@sentry/tracing");
// const { logger, requestLogger, errorLogger } = require('./api/lib/logging')

const isDevelopment = process.env.NODE_ENV !== "production";
const app = next({ dev: isDevelopment });

module.exports = app;

// const nextHandler = app.getRequestHandler();

// Configure Sentry error tracking -- should be done as early as possible
if (process.env.SENTRY_DSN) {
    Sentry.init({
        dsn: process.env.SENTRY_DSN,
        environment: process.env.NODE_ENV,
    });
} else {
    console.log("Sentry is disabled");
}

// Configure the Keycloak client
// const keycloakClient = new Keycloak(
//     { store: memoryStore },
//     {
//         serverUrl: process.env.KEYCLOAK_URL,
//         realm: process.env.KEYCLOAK_REALM,
//         clientId: process.env.KEYCLOAK_CLIENT,
//         secret: process.env.KEYCLOAK_SECRET,
//     }
// );

const getLanding = (req, res) => {
    if (
        process.env.API_SIMPLE_TOKEN ||
        process.env.DEV_AUTH_TOKEN ||
        req.isAuthenticated()
    ) {
        console.log("going home");
        app.render(req, res, "/home");
    } else {
        app.render(req, res, "/welcome");
        console.log("going welcome");
    }
};

const router = express.Router();
const nextHandler = app.getRequestHandler();

// Handle when user lands on base url
router.get("/", bodyParser.json(), getLanding);

// Public routes
router.get(
    ["/_next/*", "/*.(svg|ico|png|gif|jpg)", "/welcome"],
    bodyParser.json(),
    nextHandler
);

app.prepare()
    .then(() => {
        const server = express();

        // Setup logging
        // server.use(errorLogger)
        // server.use(requestLogger)

        // Setup Sentry error handling
        if (process.env.SENTRY_DSN)
            server.use(Sentry.Handlers.requestHandler());

        // Support CORS requests
        server.use(cors());
        server.use(cookieParser());

        // Configure Express behind SSL proxy: https://expressjs.com/en/guide/behind-proxies.html
        // Also set "proxy_set_header X-Forwarded-Proto https;" in NGINX config
        server.set("trust proxy", true);

        auth.middleware(server);

        // Setup API client for use by getServerSideProps()
        server.use((req, _, next) => {
            let token;
            if (process.env.API_SIMPLE_TOKEN)
                // development with mock API
                token = process.env.API_SIMPLE_TOKEN;
            else if (process.env.DEV_AUTH_TOKEN)
                // development with OAuth token
                token = process.env.DEV_AUTH_TOKEN;
            else token = req.user ? req.user.accessToken : null; // set by Keycloak/Globus in auth.middleware() or undefined for mock API

            req.api = new DataWatchAPI({
                baseUrl: process.env.API_BASE_URL,
                token: token,
                debug: true,
            });

            req.isDevelopment = isDevelopment;
            next();
        });

        server.use("/", router);

        // // Support JSON encoded request bodies
        // server.use(bodyParser.json());

        // // Configure sessions
        // server.use(
        //     session({
        //         store: memoryStore,
        //         secret: process.env.SESSION_SECRET,
        //         resave: false,
        //         saveUninitialized: true,
        //         //FIXME This causes "Could not obtain grant code: Error: 400:Bad Request"
        //         // cookie: {
        //         //     secure: true
        //         // }
        //     })
        // );

        // // Configure Keycloak
        // server.use(keycloakClient.middleware({ logout: "/logout" }));

        // // For "sign in" button on landing page
        // server.get("/login", keycloakClient.protect(), (_, res) => {
        //     res.redirect("/");
        // });

        // server.get("*", keycloakClient.checkSso(), (req, res) => {
        //     const token = req?.kauth?.grant?.access_token?.token;

        //     // Default to landing page if not logged in
        //     if (!token) app.render(req, res, "/welcome");
        //     else {
        //         // Setup API client for use in getServerSideProps()
        //         req.api = new API({
        //             baseUrl: process.env.API_BASE_URL,
        //             token,
        //         });

        //         // Pass to Next.js
        //         return nextHandler(req, res);
        //     }
        // });

        // Catch errors
        if (process.env.SENTRY_DSN) server.use(Sentry.Handlers.errorHandler());

        server.listen(process.env.PORT, (err) => {
            if (err) throw err;
            if (isDevelopment)
                console.log("!!!!!!!!! RUNNING IN DEV MODE !!!!!!!!!!");
            console.log(`Ready on port ${process.env.PORT}`);
        });
    })
    .catch((exception) => {
        console.error(exception.stack);
        process.exit(1);
    });

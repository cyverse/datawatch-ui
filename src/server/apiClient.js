/*
 * Cacoa API Client Adapter
 *
 * Could someday be turned into an npm package for external users.
 *
 * OpenAPI Specification: https://gitlab.com/cyverse/nafigos/-/blob/master/docs/openapi/openapi.yaml
 *
 */

const axios = require("axios");
const qs = require("qs");
class DataWatchAPI {
    constructor(params) {
        this.baseUrl = params ? params.baseUrl : "/api";
        this.token = params ? params.token : null;
        this.debug = params ? params.debug : false;
    }

    request(options) {
        if (!options.headers) options.headers = {};
        options.headers["Content-Type"] = "application/json";
        if (this.token) options.headers["Authorization"] = this.token;

        options.timeout = 30 * 1000;

        // Add custom parameter serializer to encode spaces with %20 instead of '+'
        options.paramsSerializer = (params) =>
            qs.stringify(params, { arrayFormat: "repeat" });

        if (this.debug)
            console.log(`axios request: ${options.method} ${options.url}`);
        return axios.request(options);
    }

    async get(path, params) {
        // params can contain optional "offset" & "limit" properties
        const res = await this.request({
            method: "get",
            url: `${this.baseUrl}${path}`,
            params,
        });
        console.log(res.data)
        return res.data;
    }

    async put(path, data) {
        const res = await this.request({
            method: "put",
            url: `${this.baseUrl}${path}`,
            data,
        });
        return res.data;
    }

    async post(path, params) {
        const res = await this.request({
            method: "post",
            url: `${this.baseUrl}${path}`,
            params,
        });
        return res.params;
    }

    async patch(path, data) {
        const res = await this.request({
            method: "patch",
            url: `${this.baseUrl}${path}`,
            data,
        });
        return res.data;
    }

    async delete(path) {
        const res = await this.request({
            method: "delete",
            url: `${this.baseUrl}${path}`,
        });
        return res.data;
    }

    /*
     * Listener endpoints
     */

    async listeners(params) {
        return await this.get(`/listeners`, params);
    }

    async createListener(params) {
        return await this.post(`/listeners`, params);
    }

    async listener(id) {
        return await this.get(`/listeners/${id}`);
    }

    async deleteListener(id) {
        return await this.delete(`/listeners/${id}`);
    }

    /*
     * User endpoints
     */

    async user(id) {
        return await this.get(`/users/${id}`);
    }

    /*
     * Utility functions
     */

    errorMessage(err) {
        return err?.response?.data?.message || err?.message;
    }
}

module.exports = DataWatchAPI;

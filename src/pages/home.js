import { Stack } from "@mui/material";
import Layout from "../components/Layout";
import WelcomeBanner from "../components/WelcomeBanner";
import { useUser } from "../contexts";
import LoginForm from "../components/LoginForm";

const Page = (props) => {
    const [user] = useUser();
    return (
        <Layout title="Home">
            <WelcomeBanner />
            <Stack mt={4}>
                DataWatch can notify you of file events from the DataStore.
                Notifications can be in the form of an email or an HTTP request.
                {!user && <LoginForm />}
            </Stack>
        </Layout>
    );
};

export default Page;

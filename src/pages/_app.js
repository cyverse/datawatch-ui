import React from "react";
import Head from "next/head";
import getConfig from "next/config";
import { ThemeProvider, StyledEngineProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import { ConfigProvider } from "../contexts/config";
import { APIProvider, ErrorProvider, UserProvider } from "../contexts";
import theme from "../theme";
// import { CookiesProvider } from 'react-cookie' //FIXME do not use this module!  Caused problems with WelcomeBanner in User Portal
export default function MyApp(props) {
    const { Component, pageProps, isDevelopment, user, baseUrl, token } = props;
    const config = getConfig().publicRuntimeConfig;
    config.isDevelopment = isDevelopment;

    React.useEffect(() => {
        // Remove the server-side injected CSS.
        const jssStyles = document.querySelector("#jss-server-side");
        if (jssStyles) {
            jssStyles.parentElement.removeChild(jssStyles);
        }
    }, []);

    // const theme =
    //   config.THEME && themes[config.THEME]
    //     ? themes[config.THEME]
    //     : themes.default;

    return (
        <>
            <Head>
                <title>CyVerse DataWatch</title>
                <meta
                    name="viewport"
                    content="minimum-scale=1, initial-scale=1, width=device-width"
                />
            </Head>
            <StyledEngineProvider injectFirst>
                <ThemeProvider theme={theme}>
                    <CssBaseline />
                    <ConfigProvider config={config}>
                        <APIProvider baseUrl={baseUrl} token={token}>
                            <UserProvider user={user}>
                                <ErrorProvider>
                                    <Component {...pageProps} />
                                </ErrorProvider>
                            </UserProvider>
                        </APIProvider>
                    </ConfigProvider>
                </ThemeProvider>
            </StyledEngineProvider>
        </>
    );
}

MyApp.getInitialProps = async ({ Component, ctx }) => {
    const req = ctx.req;
    const api = req && req.api;
    // const username = process.env.DEV_USERNAME;

    return {
        isDevelopment: req && req.isDevelopment,
        baseUrl: api && api.baseUrl,
        token: api && api.token, //FIXME still necessary?
        // user: api && api.token && (await api.user(username)),
        pageProps: Component.getInitialProps
            ? await Component.getInitialProps(ctx)
            : {},
    };
};

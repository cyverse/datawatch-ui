import { useState, useEffect } from "react";
import {
    Grid,
    Box,
    Menu,
    MenuItem,
    Button,
    Typography,
    TextField,
    Divider,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    Backdrop,
    CircularProgress,
    Tooltip,
} from "@mui/material";
import { makeStyles } from "@mui/styles";
import { Add as AddIcon } from "@mui/icons-material";
import Layout from "../components/Layout";
import ConfirmationDialog from "../components/ConfirmationDialog";
import ListenerCard from "../components/cards/ListenerCard";
import { useAPI, useError } from "../contexts";

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: "3em",
        marginBottom: "2em",
    },
    input: {
        fontSize: "0.9rem",
        minWidth: "10rem",
    },
    input2: {
        fontSize: "0.9rem",
        minWidth: "20rem",
    },
    backdrop: {
        zIndex: theme.zIndex.drawer + 999,
        color: "#fff",
    },
}));

const typeLabel = (type) => {
    switch (type) {
        case "email":
            return "Email";
        case "webdav":
            return "WebDav";
        case "webhook":
            return "Web Hook";
        default:
            return "unknown type";
    }
};

const Listeners = (props) => {
    const classes = useStyles();
    const api = useAPI();
    const [busy, setBusy] = useState(false);
    const [_, setError] = useError();
    const [listeners, setListeners] = useState(props.listeners);
    const [deleteId, setDeleteId] = useState();
    const [addListener, setAddListener] = useState();
    const [anchorEl, setAnchorEl] = useState(null);
    const handleError = (msg, error) => {
        console.error(msg, error);
        setBusy(false);
        setError(
            <>
                {msg}
                {error && (
                    <Box mt={2}>
                        Details:{" "}
                        {error.response ? error.response.data : error.message}
                    </Box>
                )}
            </>
        );
    };

    const fetchListeners = async () => {
        try {
            const res = await api.listeners();
            setListeners(res);
            console.log(res);
        } catch (error) {
            handleError("Could not retrieve listeners list", error);
        } finally {
            setBusy(false);
        }
    };

    const deleteListener = async (id) => {
        setBusy(true);
        try {
            await api.deleteListener(id);
            setTimeout(fetchListeners, 500);
        } catch (error) {
            handleError("Could not delete listener", error);
        }
    };

    const createListener = async (values) => {
        // const listId = values["name"];
        const listType = values["listenerTypeID"];
        values["path"] = values["path"].split("\n");
        let req;
        if (listType === "webdav") {
            req = {
                // name: values["name"],
                listenerTypeID: listType,
                notifyInterval: values["notifyInterval"],
                sourceID: values["sourceID"],
                path: values["path"],
                url: values["url"],
                authentication: values["username"] + ":" + values["password"],
            };
        } else {
            req = values;
        }

        console.log("submit:", req);
        await api.createListener(req);
        setTimeout(fetchListeners, 500);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <Layout title="Listeners">
            <br />
            <Box display="flex" pb={1}>
                <Box flexGrow={1}>
                    <Typography variant="h6">My Listeners</Typography>
                </Box>
                <div>
                    <Button
                        aria-controls="listeners-menu"
                        aria-haspopup="true"
                        onClick={(e) => setAnchorEl(e.currentTarget)}
                        color="primary"
                        variant="contained"
                        startIcon={<AddIcon />}
                    >
                        Add Listener
                    </Button>
                    <Menu
                        id="listeners-menu"
                        anchorEl={anchorEl}
                        keepMounted
                        open={Boolean(anchorEl)}
                        onClose={handleClose}
                    >
                        <MenuItem
                            onClick={() =>
                                handleClose() || setAddListener("email")
                            }
                        >
                            Email
                        </MenuItem>
                        <MenuItem
                            onClick={() =>
                                handleClose() || setAddListener("webdav")
                            }
                        >
                            WebDAV
                        </MenuItem>
                        <MenuItem
                            onClick={() =>
                                handleClose() || setAddListener("webhook")
                            }
                        >
                            Webhook
                        </MenuItem>
                    </Menu>
                </div>
            </Box>
            <Divider />
            <Box mt={2}>
                {listeners && listeners.length > 0 ? (
                    <Grid
                        // make every card have the same size
                        container
                        spacing={2}
                        justifyContent="flex-start"
                        alignItems="stretch"
                    >
                        {/* // spacing={3}
                        // direction="row"
                        // jusitfy="flex-end"
                        // alignItems="stretch" */}

                        {listeners.map((listener, index) => (
                            <Grid
                                item
                                xs={12}
                                s={12}
                                md={4}
                                lg={4}
                                xl={3}
                                key={index}
                            >
                                <ListenerCard
                                    {...listener}
                                    onDelete={() => {
                                        fetchListeners();
                                        setDeleteId(listener.listenerID);
                                        console.log(listener.listenerID);
                                    }}

                                    //onChange={() => setChecked(enabled => !enabled)}
                                />
                            </Grid>
                        ))}
                    </Grid>
                ) : (
                    <Typography>
                        You have not created a listener yet.
                    </Typography>
                )}
            </Box>
            <ConfirmationDialog
                open={!!deleteId}
                title="Delete Listener"
                handleClose={() => setDeleteId()}
                handleSubmit={() => deleteListener(deleteId)}
            />
            {addListener && (
                <AddListenerDialog
                    open={!!addListener}
                    type={addListener}
                    handleClose={() => setAddListener()}
                    handleSubmit={createListener}
                />
            )}
            <Backdrop className={classes.backdrop} open={busy}>
                <CircularProgress color="inherit" />
            </Backdrop>
        </Layout>
    );
};
const AddListenerDialog = ({ open, type, handleClose, handleSubmit }) => {
    const [values, setValues] = useState({});
    const [isValid, setValid] = useState(false);
    // const [hasConflict, setHasConflict] = useState(false);

    const handleChange = (e) => {
        console.log("Handle change", e.target);
        console.log(values);
        setValues({
            ...values,
            [e.target.id || e.target.name]: e.target.value,
            listenerTypeID: type,
        });
    };

    useEffect(() => {
        const validate = (values) => {
            if (type === "email")
                return (
                    // values["name"] &&
                    values["notifyInterval"] &&
                    values["sourceID"] &&
                    values["path"] &&
                    values["email"]
                );
            if (type === "webdav")
                return (
                    // values["name"] &&
                    values["notifyInterval"] &&
                    values["sourceID"] &&
                    values["path"] &&
                    values["url"] &&
                    values["username"] &&
                    values["password"]
                );
            if (type === "webhook")
                return (
                    // values["name"] &&
                    values["notifyInterval"] &&
                    values["url"] &&
                    values["sourceID"] &&
                    values["path"] &&
                    values["method"]
                );
        };
        setValid(validate(values));
        // setHasConflict(listeners.some((c) => c.name === values["name"]));
    }, [type, values]);

    return (
        <Dialog open={open} onClose={handleClose} maxWidth="sm">
            <DialogTitle>Add {typeLabel(type)} Listener</DialogTitle>
            <DialogContent>
                {type === "email" && <AddEmailForm onChange={handleChange} />}
                {type === "webdav" && <AddWebDavForm onChange={handleChange} />}
                {type === "webhook" && (
                    <AddWebhookForm onChange={handleChange} />
                )}
                {/* <Box
                    my={2}
                    display="flex"
                    sx={{ color: "red", height: "2em", maxWidth: "22em" }}
                >
                    {hasConflict && (
                        <Typography>
                            WARNING: a listener with this name already exists
                            and will be overwritten
                        </Typography>
                    )}
                </Box> */}
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose}>Cancel</Button>
                <Button
                    disabled={!isValid}
                    color="primary"
                    variant="contained"
                    onClick={() => {
                        handleSubmit(values);
                        handleClose();
                    }}
                >
                    Add
                </Button>
            </DialogActions>
        </Dialog>
    );
};

const AddEmailForm = ({ onChange }) => {
    const classes = useStyles();

    return (
        <>
            {/* <Box
                mb={2}
                sx={{
                    width: "22em",
                    maxWidth: "100%",
                }}
            >
                <Tooltip title="The name of the listener">
                    <TextField
                        id="name"
                        label="Name"
                        required={true}
                        placeholder="My email listener"
                        size="small"
                        variant="outlined"
                        fullWidth
                        InputLabelProps={{
                            shrink: true,
                            className: classes.input,
                        }}
                        onChange={onChange}
                    />
                </Tooltip>
            </Box> */}
            <Box mb={2} mt={2}>
                <Tooltip title="The interval in seconds to check for new events">
                    <TextField
                        id="notifyInterval"
                        label="Notify interval"
                        // Note: this values are not real, they are just for demo
                        placeholder="100"
                        size="small"
                        variant="outlined"
                        fullWidth
                        type="number"
                        InputProps={{
                            className: classes.input,
                        }}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        onChange={onChange}
                    />
                </Tooltip>
            </Box>

            <Box mb={2}>
                <TextField
                    id="sourceID"
                    label="Source ID"
                    // Note: this values are not real, they are just for demo
                    placeholder="cyverse"
                    size="small"
                    variant="outlined"
                    fullWidth
                    InputProps={{
                        className: classes.input,
                    }}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    onChange={onChange}
                />
            </Box>
            <Box mb={2}>
                <TextField
                    id="path"
                    label="Path(s)"
                    // Note: this values are not real, they are just for demo
                    placeholder="/path/to/file"
                    multiline
                    size="small"
                    variant="outlined"
                    fullWidth
                    InputProps={{
                        className: classes.input,
                    }}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    onChange={onChange}
                />
            </Box>

            <Box mb={2}>
                <Tooltip title="The email address to send notifications to">
                    <TextField
                        id="email"
                        label="Email"
                        // this value is optional
                        placeholder="johndoe@example.com"
                        size="small"
                        variant="outlined"
                        fullWidth
                        InputLabelProps={{
                            shrink: true,
                            className: classes.input,
                        }}
                        onChange={onChange}
                    />
                </Tooltip>
            </Box>
        </>
    );
};

const AddWebDavForm = ({ onChange }) => {
    const classes = useStyles();

    return (
        <>
            <Box
                mb={2}
                sx={{
                    width: "30em",
                    maxWidth: "100%",
                }}
            >
                <TextField
                    id="name"
                    label="Name"
                    required={true}
                    placeholder="My webdav listener"
                    size="small"
                    variant="outlined"
                    fullWidth
                    InputLabelProps={{
                        shrink: true,
                        className: classes.input,
                    }}
                    onChange={onChange}
                />
            </Box>
            <Box mb={2}>
                <Tooltip title="The interval in seconds to check for new events">
                    <TextField
                        id="notifyInterval"
                        label="Notify interval"
                        // Note: this values are not real, they are just for demo
                        placeholder="100"
                        size="small"
                        variant="outlined"
                        fullWidth
                        type="number"
                        InputProps={{
                            className: classes.input,
                        }}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        onChange={onChange}
                    />
                </Tooltip>
            </Box>
            <Box mb={2}>
                <TextField
                    id="sourceID"
                    label="Source ID"
                    // Note: this values are not real, they are just for demo
                    placeholder="cyverse"
                    size="small"
                    variant="outlined"
                    fullWidth
                    InputProps={{
                        className: classes.input,
                    }}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    onChange={onChange}
                />
            </Box>
            <Box mb={2}>
                <TextField
                    id="path"
                    label="Path(s)"
                    // Note: this values are not real, they are just for demo
                    placeholder="/path/to/file"
                    multiline
                    size="small"
                    variant="outlined"
                    fullWidth
                    InputProps={{
                        className: classes.input,
                    }}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    onChange={onChange}
                />
            </Box>
            <Box mb={2}>
                <Tooltip title="The URL of the webdav server">
                    <TextField
                        id="url"
                        label="URL"
                        required={true}
                        placeholder="https://www.example.com/dav/home/user/files"
                        size="small"
                        variant="outlined"
                        fullWidth
                        InputLabelProps={{
                            shrink: true,
                            className: classes.input,
                        }}
                        onChange={onChange}
                    />
                </Tooltip>
            </Box>
            {/* create two boxes inside a box, the first inside box is for the user name and the second for the password */}

            <Box
                id="authentication-box"
                sx={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "left",
                    marginTop: "10px",
                    marginBottom: "10px",
                }}
            >
                <Typography variant="subtitle1" gutterBottom>
                    Authentication
                </Typography>

                <Box mb={2}>
                    <TextField
                        id="username"
                        label="Username"
                        required={true}
                        placeholder="user"
                        size="small"
                        variant="outlined"
                        fullWidth
                        InputLabelProps={{
                            shrink: true,
                            className: classes.input,
                        }}
                        onChange={onChange}
                    />
                </Box>
                <Box mb={2}>
                    <TextField
                        id="password"
                        label="Password"
                        required={true}
                        placeholder="password"
                        size="small"
                        variant="outlined"
                        fullWidth
                        type="password"
                        InputLabelProps={{
                            shrink: true,
                            className: classes.input,
                        }}
                        onChange={onChange}
                    />
                </Box>
            </Box>
        </>
    );
};

const AddWebhookForm = ({ onChange }) => {
    const classes = useStyles();
    const methods = [
        {
            value: "GET",
            label: "GET",
        },
        {
            value: "PUT",
            label: "PUT",
        },
        {
            value: "POST",
            label: "POST",
        },
    ];

    return (
        <>
            <Box
                mb={2}
                sx={{
                    width: "30em",
                    maxWidth: "100%",
                }}
            >
                <TextField
                    id="name"
                    label="Name"
                    required={true}
                    placeholder="My webhook listener"
                    size="small"
                    variant="outlined"
                    fullWidth
                    InputLabelProps={{
                        shrink: true,
                        className: classes.input,
                    }}
                    onChange={onChange}
                />
            </Box>
            <Box mb={2}>
                <Tooltip title="The interval in seconds to check for new events">
                    <TextField
                        id="notifyInterval"
                        label="Notify interval"
                        // Note: this values are not real, they are just for demo
                        placeholder="100"
                        size="small"
                        variant="outlined"
                        type="number"
                        fullWidth
                        InputProps={{
                            className: classes.input,
                        }}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        onChange={onChange}
                    />
                </Tooltip>
            </Box>

            <Box mb={2}>
                <TextField
                    id="sourceID"
                    label="Source ID"
                    // Note: this values are not real, they are just for demo
                    placeholder="cyverse"
                    size="small"
                    variant="outlined"
                    fullWidth
                    InputProps={{
                        className: classes.input,
                    }}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    onChange={onChange}
                />
            </Box>
            <Box mb={2}>
                <TextField
                    id="path"
                    label="Path(s)"
                    // Note: this values are not real, they are just for demo
                    placeholder="/path/to/file"
                    multiline
                    size="small"
                    variant="outlined"
                    fullWidth
                    InputProps={{
                        className: classes.input,
                    }}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    onChange={onChange}
                />
            </Box>

            <Box mb={2}>
                <TextField
                    id="url"
                    label="URL"
                    required={true}
                    placeholder="https://www.example.com/dav/home/user/files"
                    size="small"
                    variant="outlined"
                    fullWidth
                    InputLabelProps={{
                        shrink: true,
                        className: classes.input,
                    }}
                    onChange={onChange}
                />
            </Box>
            {/* create two boxes inside a box, the first inside box is for the user name and the second for the password */}

            <Box mb={2}>
                <Tooltip title="The HTTP method to use when sending the webhook">
                    <TextField
                        id="method"
                        label="HTTP method"
                        select
                        SelectProps={{
                            id: "method-select",
                            name: "method",
                            displayEmpty: true,
                            defaultValue: "",
                        }}
                        size="small"
                        variant="outlined"
                        fullWidth
                        InputLabelProps={{
                            shrink: true,
                            className: classes.input,
                        }}
                        onChange={onChange}
                    >
                        {methods.map((option) => (
                            <option key={option.value} value={option.value}>
                                {option.label}
                            </option>
                        ))}
                    </TextField>
                </Tooltip>
            </Box>
        </>
    );
};

export async function getServerSideProps({ req }) {
    const listeners = await req.api.listeners();
    return {
        props: {
            listeners,
        },
    };
}

export default Listeners;

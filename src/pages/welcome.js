import { Grid, Button } from "@mui/material";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles((theme) => ({
    loginButton: {
        width: "10em",
        fontWeight: "bold",
    },
}));

const Welcome = (props) => {
    const classes = useStyles();

    return (
        <Grid
            container
            spacing={4}
            direction="column"
            alignItems="center"
            justifyContent="center"
            style={{ minHeight: "90vh" }}
        >
            <Grid item>{"<DataWatch Logo>"}</Grid>
            <Grid item>
                <Button
                    variant="contained"
                    color="primary"
                    size="large"
                    className={classes.loginButton}
                    href="/login"
                >
                    Sign-In
                </Button>
            </Grid>
        </Grid>
    );
};

export default Welcome;

import { createTheme } from "@mui/material/styles";

const theme = createTheme({
    breakpoints: {
        values: {
            xs: 0,
            sm: 600,
            md: 960,
            lg: 1280,
            xl: 1920,
        },
    },
    palette: {
        mode: "light",
        primary: {
            light: "#0ea1f2",
            main: "#0971ab",
            dark: "#054162",
            contrastText: "#fff",
        },
        secondary: {
            light: "#db0c91",
            main: "#ab0971",
            dark: "#7b0651",
            contrastText: "#fff",
        },
        rose: {
            main: "#f50057",
        },
        appbar: {
            main: "#38006b",
            light: "#9c4dcc",
            dark: "#38006b",
        },
        topbar: {
            light: "#9c4dcc",
            main: "#6a1b9a",
            dark: "#38006b",
        },
        error: {
            light: "#e57373",
            main: "#f43648",
            dark: "#9A0036",
            contrastText: "#fff",
        },
        success: {
            light: "#81C784",
            main: "#4caf50",
            dark: "#388E3C",
            contrastText: "rgba(0,0,0,.87)",
        },
        warning: {
            light: "#ffb74d",
            main: "#ff9800",
            dark: "#f57c00",
            contrastText: "rgba(0,0,0,.87)",
        },
        divider: "rgba(0,0,0,.12)",
        background: {
            default: "#eeeeee",
        },
    },
    overrides: {
        MuiAvatar: {
            colorDefault: {
                color: "#ffff",
                backgroundColor: "#0971ab",
            },
        },
        MuiListItem: {
            secondaryAction: {
                text: {
                    color: "#fffff",
                },
            },
            root: {
                "& .MuiSvgIcon-root": {
                    fill: "white",
                },
                "&$selected": {
                    backgroundColor: "#0971ab",
                    color: "#ffffff",
                    "&:hover": {
                        backgroundColor: "#0971ab",
                    },
                    "& .MuiSvgIcon-root": {
                        fill: "white",
                    },
                },
            },
        },
    },
});

export default theme;

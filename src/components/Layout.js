import { Backdrop, CircularProgress } from "@mui/material";
import Dashboard from "./Dashboard";

const Layout = (props) => {
    return (
        <div>
            <Dashboard {...props} />
            <Backdrop
                open={!!props.busy}
                style={{ zIndex: 999, color: "#fff" }}
            >
                <CircularProgress color="inherit" />
            </Backdrop>
        </div>
    );
};

export default Layout;

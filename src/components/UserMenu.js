import React from "react";
import { makeStyles, withStyles } from "@mui/styles";
import { AccountCircle as PersonIcon } from "@mui/icons-material";
import {
    Link,
    Typography,
    Menu,
    Grid,
    Avatar,
    IconButton,
    Button,
    Divider,
    Paper,
    Tooltip,
} from "@mui/material";

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(1),
        [theme.breakpoints.down("sm")]: {
            padding: theme.spacing(0.2),
            backgroundColor: theme.palette.info.main,
        },
    },
    typography: {
        fontSize: "smaller",
        color: "#282828", //theme.palette.info.main, //FIXME fix in palette
        [theme.breakpoints.down("sm")]: {
            color: theme.palette.info.contrastText,
        },
    },
    divider: {
        margin: theme.spacing(1),
        [theme.breakpoints.down("sm")]: {
            margin: theme.spacing(0.5),
        },
    },
    button: {
        color: theme.palette.primary.main,
        [theme.breakpoints.down("sm")]: {
            color: theme.palette.info.contrastText,
        },
    },
}));

const StyledMenu = withStyles({
    paper: {
        border: "1px solid #d3d4d5",
        minWidth: "15em",
    },
})((props) => (
    <Menu
        elevation={0}
        anchorOrigin={{
            vertical: "bottom",
            horizontal: "center",
        }}
        transformOrigin={{
            vertical: "top",
            horizontal: "center",
        }}
        {...props}
    />
));

// const StyledMenuItem = withStyles(theme => ({
//     root: {
//         '&:focus': {
//             backgroundColor: theme.palette.primary.main,
//             '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
//                 color: theme.palette.common.white,
//             },
//         },
//     },
// }))(MenuItem)

export default function UserMenu({ user }) {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);

    return (
        <div>
            <Tooltip title="Account Info">
                <IconButton
                    color="inherit"
                    aria-label="account"
                    aria-controls="customized-menu"
                    aria-haspopup="true"
                    onClick={(event) => setAnchorEl(event.currentTarget)}
                    size="large"
                >
                    <PersonIcon fontSize="large" />
                </IconButton>
            </Tooltip>
            <StyledMenu
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={() => setAnchorEl(null)}
            >
                <Paper elevation={0} className={classes.paper}>
                    <Grid
                        container
                        direction="column"
                        justifyContent="center"
                        alignItems="center"
                        spacing={1}
                    >
                        <Avatar>
                            {user?.username.charAt(0).toUpperCase()}
                        </Avatar>
                        <Grid item>
                            <Typography
                                variant="caption"
                                className={classes.typography}
                            >
                                {user?.username}
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography
                                variant="caption"
                                className={classes.typography}
                            >
                                {user?.primary_email}
                            </Typography>
                        </Grid>
                        {/* <Grid item>
                            <Button
                                variant="outlined"
                                className={classes.button}
                                size="small"
                            >
                                Manage account
                            </Button>
                        </Grid> */}
                    </Grid>
                    <Divider className={classes.divider} />
                    <Grid
                        container
                        direction="column"
                        justifyContent="center"
                        alignItems="center"
                        spacing={1}
                    >
                        <Grid item>
                            <Button
                                variant="outlined"
                                className={classes.button}
                                size="small"
                                href="/logout"
                            >
                                logout
                            </Button>
                        </Grid>
                    </Grid>
                    <Divider className={classes.divider} />
                    <Grid
                        container
                        direction="row"
                        justifyContent="center"
                        alignItems="center"
                        spacing={1}
                    >
                        <Grid item>
                            <Link
                                className={classes.typography}
                                href="https://cyverse.org/policies"
                                target="_blank"
                            >
                                policies
                            </Link>
                        </Grid>
                        <Grid item>
                            <Typography variant="caption">•</Typography>
                        </Grid>
                        <Grid item>
                            <Link
                                className={classes.typography}
                                href="https://cyverse.org/about"
                                target="_blank"
                            >
                                about
                            </Link>
                        </Grid>
                    </Grid>
                </Paper>
            </StyledMenu>
        </div>
    );
}

import { Button } from "@mui/base";
import { Grid, TextField } from "@mui/material";

export default function LoginForm(props) {
    return (
        <Grid
            container
            alignItems="center"
            direction="column"
            justifyContent="center"
            mt={4}
            spacing={2}
        >
            <Grid item>
                <TextField
                    fullWidth
                    label="Username"
                    name="username"
                    variant="outlined"
                />
            </Grid>
            <Grid item xs={6}>
                <TextField
                    variant="outlined"
                    fullWidth
                    label="Password"
                    name="password"
                    type="password"
                />
            </Grid>
            <Grid item xs={6}>
                <Button>Login</Button>
            </Grid>
        </Grid>
    );
}

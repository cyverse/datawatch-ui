import React from "react";
import {
    Avatar,
    Card,
    CardHeader,
    CardContent,
    Divider,
    FormControl,
    FormControlLabel,
    Grid,
    IconButton,
    Switch,
    Typography,
} from "@mui/material";
import { Delete as DeleteIcon, Edit as EditIcon } from "@mui/icons-material";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles((theme) => ({
    rose: {
        color: "theme.palette.rose.main",
    },
    media: {
        height: 0,
        paddingTop: "56.25%", // 16:9
    },
    expand: {
        transform: "rotate(0deg)",
        marginLeft: "auto",
        transition: theme.transitions.create("transform", {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: "rotate(180deg)",
    },
    title: {
        lineHeight: "1.1",
        fontSize: "1.4em",
    },
    cardActions: {
        flexDirection: "column",
        alignItems: "flex-start",
    },
}));

const ListenerCardContents = ({
    email,
    url,
    listenerTypeID,
    enabled,
    paths,
    method,
}) => {
    if (listenerTypeID === "email") {
        return (
            <Typography variant="caption">
                Number of paths: {paths.length}
                <br />
                e-mail: {email}
                <br />
            </Typography>
        );
    } else if (listenerTypeID === "webdav") {
        return (
            <Typography variant="caption">
                Number of paths: {paths.length}
                <br />
                URL: {url}
                <br />
            </Typography>
        );
    } else {
        return (
            <Typography variant="caption">
                Number of paths: {paths.length}
                <br />
                URL: {url}
                <br />
                Method: {method}
                <br />
            </Typography>
        );
    }
};

const ListenerCard = ({
    enabled,
    // name,
    listenerTypeID,
    email,
    url,
    paths,
    method,
    onDelete,
}) => {
    const classes = useStyles();
    return (
        <Card>
            <CardHeader
                className={classes.headers}
                avatar={<Avatar>ST</Avatar>}
                title={listenerTypeID}
                // title={name}
                titleTypographyProps={{ className: classes.title }}
                // subheader={
                //     <Typography variant="caption">
                //         Type: {listenerTypeID}
                //         {/* Created: {created_at} */}
                //     </Typography>
                // }
                action={
                    <Grid container alignItems="center">
                        <FormControl variant="outlined">
                            <FormControlLabel
                                control={
                                    <Switch
                                        checked={enabled}
                                        onChange={(e) => {
                                            //   wizard.onChange("config_boot_disk", e.target.checked)
                                        }}
                                    />
                                }
                                label="Enabled"
                            />
                        </FormControl>
                        <Divider orientation="vertical" flexItem />
                        <Grid item>
                            <IconButton
                                onClick={(e) => {
                                    e.preventDefault();
                                }}
                            >
                                <EditIcon />
                            </IconButton>
                        </Grid>
                        <Grid item>
                            <IconButton
                                onClick={(e) => {
                                    e.preventDefault();
                                }}
                            >
                                <DeleteIcon />
                            </IconButton>
                        </Grid>
                    </Grid>
                }
            />

            <CardContent>
                <ListenerCardContents
                    email={email}
                    url={url}
                    listenerTypeID={listenerTypeID}
                    paths={paths}
                    method={method}
                />
            </CardContent>
        </Card>
    );
};

export default ListenerCard;
